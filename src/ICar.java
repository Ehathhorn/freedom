
public interface ICar {
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
